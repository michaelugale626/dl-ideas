<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoList;
use App\Http\Controllers\UserManager;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/list', [TodoList::class, 'index']);
Route::get('/hello/{name}', [TodoList::class, 'showHello']);


Route::group([ 'prefix' => 'admin'], function () {

    Route::get('/dashboard', [UserManager::class, 'showPage']);
    Route::get('/listing', [UserManager::class, 'showListing']);
    Route::get('/create', [UserManager::class, 'showCreate']);
    Route::get('/edit/{id}', [UserManager::class, 'showEdit']);
    Route::get('/delete/{id}', [UserManager::class, 'processDelete']);
    Route::post('/create', [UserManager::class, 'processCreate']);
    Route::post('/update', [UserManager::class, 'processUpdate']);

    Route::group([ 'prefix' => 'assignment'], function () {
        Route::get('/list', [TodoList::class, 'showPage']);
    });

    Route::group([ 'prefix' => 'task'], function () {
        Route::get('/listing', [TaskController::class, 'showListing']);
        Route::get('/create', [TaskController::class, 'showCreate']);
        Route::get('/edit/{id}', [TaskController::class, 'showEdit']);
        Route::get('/delete/{id}', [TaskController::class, 'processDelete']);
        Route::post('/create', [TaskController::class, 'processCreate']);
        Route::post('/update', [TaskController::class, 'processUpdate']);
    });

    Route::group([ 'prefix' => 'category'], function () {
        Route::get('/listing', [CategoryController::class, 'showListing']);
        Route::get('/create', [CategoryController::class, 'showCreate']);
        Route::get('/edit/{id}', [CategoryController::class, 'showEdit']);
        Route::get('/delete/{id}', [CategoryController::class, 'processDelete']);
        Route::post('/create', [CategoryController::class, 'processCreate']);
        Route::post('/update', [CategoryController::class, 'processUpdate']);
    });
});