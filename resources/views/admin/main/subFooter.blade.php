@section('subFooterContent')

    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; Michael Ugale 2021</span>
            </div>
        </div>
    </footer>

@stop