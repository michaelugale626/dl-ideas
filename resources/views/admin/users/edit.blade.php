@extends('admin/main.master')
@include('admin/main.warning')

@push('mainCSS')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit {{ $item->first_name }} {{ $item->last_name }}</h1>
    </div>

    <div class="col-lg-6">

        @yield('warningContent')

        {{ Form::open(array('url' => 'admin/update', 'method' => 'post', 'class' =>'user')) }}
            @csrf
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    {{ Form::text('firstname', $item->first_name, ['class'=>'form-control form-control-user', 'placeholder' => 'First Name']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('lastname', $item->last_name, ['class'=>'form-control form-control-user', 'placeholder' => 'Last Name']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::email('email', $item->email, ['class'=>'form-control form-control-user', 'placeholder' => 'Email Address']) }}
            </div>
            {{ Form::hidden('itemid', $item->id) }}
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        {{ Form::close() }}

    </div>

@endpush

@push('mainScripts')

@endpush