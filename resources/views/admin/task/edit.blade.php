@extends('admin/main.master')
@include('admin/main.warning')

@push('mainCSS')

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit {{ $data['item']->name }}</h1>
    </div>

    <div class="col-lg-6">

        @yield('warningContent')

        {{ Form::open(array('url' => 'admin/task/update', 'method' => 'post', 'class' =>'user')) }}
            @csrf
            <div class="form-group">
                <label for="exampleInputCategory">Task Name</label>
                {{ Form::text('task', $data['item']->name, ['class'=>'form-control', 'placeholder' => 'write a task name']) }}
            </div>
            <div class="form-group">
                <label for="exampleInputCategory">Category</label>
                {{ Form::select('category', $data['list'],  $data['item']->category_id, ['class' => 'form-control ']) }}
            </div>
            <div class="form-group">
                <label for="exampleInputDesc">Description</label>
                {!! Form::textarea('description', $data['item']->description, ['class' => 'form-control', 'rows' => 4, 'cols' => 54]) !!}
            </div>
            {{ Form::hidden('itemid', $data['item']->id) }}
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        {{ Form::close() }}

    </div>

@endpush

@push('mainScripts')

@endpush