@extends('admin/main.master')
@include('admin/main.warning')

@push('mainCSS')

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Create Task</h1>
    </div>

    <div class="col-lg-6">

        @yield('warningContent')

        {{ Form::open(array('url' => 'admin/task/create', 'method' => 'post', 'class' =>'user')) }}
            @csrf
            <div class="form-group">
                <label for="exampleInputCategory">Task Name</label>
                {{ Form::text('task', null, ['class'=>'form-control', 'placeholder' => 'write a task name']) }}
            </div>
            <div class="form-group">
                <label for="exampleInputCategory">Category</label>
                {{ Form::select('category', $list, 0, ['class' => 'form-control ']) }}
            </div>
            <div class="form-group">
                <label for="exampleInputDesc">Description</label>
                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 54]) !!}
            </div>
            {{ Form::submit('Add Category', ['class' => 'btn btn-primary']) }}
        {{ Form::close() }}

    </div>

@endpush

@push('mainScripts')

@endpush