@extends('admin/main.master')
@include('admin/main.warning')

@push('mainCSS')

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Create Category</h1>
    </div>

    <div class="col-lg-6">

        <div class="col-lg-6">

            @yield('warningContent')

            {{ Form::open(array('url' => 'admin/category/create', 'method' => 'post', 'class' =>'user')) }}
                @csrf
                <div class="form-group">
                    {{ Form::text('category', old('category'), ['class'=>'form-control form-control-user', 'placeholder' => 'Category']) }}
                </div>
                {{ Form::submit('Add Category', ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}

        </div>


    </div>

@endpush

@push('mainScripts')

@endpush