@extends('admin/main.master')
@include('admin/main.warning')

@push('mainCSS')

    <meta name="csrf-token" content="{{ csrf_token() }}">

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit {{ $item->category }}</h1>
    </div>

    <div class="col-lg-6">

        @yield('warningContent')

        {{ Form::open(array('url' => 'admin/category/update', 'method' => 'post', 'class' =>'user')) }}
            @csrf
            <div class="form-group row">
                {{ Form::text('category', $item->category, ['class'=>'form-control form-control-user', 'placeholder' => 'Category']) }}
            </div>
            {{ Form::hidden('itemid', $item->id) }}
            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
        {{ Form::close() }}

    </div>

@endpush

@push('mainScripts')

@endpush