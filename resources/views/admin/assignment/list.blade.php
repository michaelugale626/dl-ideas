@extends('admin/main.master')

@push('mainCSS')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "aaSorting": []
            });
        } );
    </script>

@endpush

@push('mainContent')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Return list of Top Completed Todo user order by number of todo. (display in Data Table)</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">List</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Count</th>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Website</th>
                        <th>Company</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($list as $row)

                        <tr>
                            <td>{{ $row['todosCount'] }}</td>
                            <td>{{ $row['data']['id'] }}</td>
                            <td>{{ $row['data']['name'] }}</td>
                            <td>{{ $row['data']['username'] }}</td>
                            <td>{{ $row['data']['email'] }}</td>
                            <td>
                                {{ $row['data']['address']['street'] }}
                                {{ $row['data']['address']['suite'] }}
                                {{ $row['data']['address']['city'] }}
                                {{ $row['data']['address']['zipcode'] }}
                            </td>
                            <td>{{ $row['data']['phone'] }}</td>
                            <td>{{ $row['data']['website'] }}</td>
                            <td>{{ $row['data']['company']['name'] }}</td>
                        </tr>

                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Count</th>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Website</th>
                        <th>Company</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endpush

@push('mainScripts')

    <script type="text/javascript" src="{{ URL::asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/demo/datatables-demo.js') }}"></script>

@endpush