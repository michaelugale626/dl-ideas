<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Interview Question</title>

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    "aaSorting": []
                });
            } );
        </script>
    </head>
    <body class="antialiased">


        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Count</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Website</th>
                    <th>Company</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($list as $row)

                    <tr>
                        <td>{{ $row['todosCount'] }}</td>
                        <td>{{ $row['data']['id'] }}</td>
                        <td>{{ $row['data']['name'] }}</td>
                        <td>{{ $row['data']['username'] }}</td>
                        <td>{{ $row['data']['email'] }}</td>
                        <td>
                            {{ $row['data']['address']['street'] }}
                            {{ $row['data']['address']['suite'] }}
                            {{ $row['data']['address']['city'] }}
                            {{ $row['data']['address']['zipcode'] }}
                        </td>
                        <td>{{ $row['data']['phone'] }}</td>
                        <td>{{ $row['data']['website'] }}</td>
                        <td>{{ $row['data']['company']['name'] }}</td>
                    </tr>

                @endforeach

            </tbody>
            <tfoot>
                <tr>
                    <th>Count</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Website</th>
                    <th>Company</th>
                </tr>
            </tfoot>
        </table>

    </body>
</html>
