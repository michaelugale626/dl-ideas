<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-26
 * Time: 21:45
 */
namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class TaskModel extends Controller
{
    protected static $table = 'task';


    /**
     * Insert new item
     *
     * @param $param
     * @return bool
     */
    public static function create($param) {

        $result = DB::table(self::$table)
            ->insert(array('name' => $param->task, 'description' => $param->description, 'category_id' => $param->category, 'created_at' => date('Y-m-d H:i:s')));

        return $result;
    }

    /**
     * update item
     *
     * @param $param
     * @return int
     */
    public static function update($param) {

        $result = DB::table(self::$table)
            ->where('id', '=',$param->itemid)
            ->update(array('name' => $param->task, 'description' => $param->description, 'category_id' => $param->category, 'updated_at' => date('Y-m-d H:i:s')));

        return $result;
    }

    /**
     * delete item
     *
     * @param $param
     * @return int
     */
    public static function delete($id) {

        $result = DB::table(self::$table)
            ->where('id', '=',$id)
            ->delete();

        return $result;

    }

    /**
     * Get all items
     *
     * @return \Illuminate\Support\Collection
     */
    public static function get() {

        $query = DB::table('task AS T')
            ->select('T.id','C.created_at','T.name','T.description','C.category','T.category_id')
            ->orderBy('T.created_at', 'desc')
            ->leftJoin('category AS C', 'C.id', '=', 'T.category_id')
            ->get();

        return $query;

    }

    /**
     * Get items details
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByID($id) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('id','=', $id)
            ->first();

        return $query;

    }

    /**
     * Check if category item exist
     *
     * @return \Illuminate\Support\Collection
     */
    public static function checkIfCategoryExist($id) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('category_id','=', $id)
            ->get();

        return (count($query)) ? true : false ;

    }
}
