<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-26
 * Time: 22:09
 */
namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class CategoryModel extends Controller
{
    protected static $table = 'category';


    /**
     * Insert new item
     *
     * @param $param
     * @return bool
     */
    public static function create($param) {

        $result = DB::table(self::$table)
            ->insert(array('category' => $param->category, 'created_at' => date('Y-m-d H:i:s')));

        return $result;
    }

    /**
     * update item
     *
     * @param $param
     * @return int
     */
    public static function update($param) {

        $result = DB::table(self::$table)
            ->where('id', '=',$param->itemid)
            ->update(array('category' => $param->category, 'updated_at' => date('Y-m-d H:i:s')));

        return $result;
    }

    /**
     * delete item
     *
     * @param $param
     * @return int
     */
    public static function delete($id) {

        $result = DB::table(self::$table)
            ->where('id', '=',$id)
            ->delete();

        return $result;

    }

    /**
     * Get all items with format
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getList() {

        $query = DB::table(self::$table)
            ->select('*')
            ->pluck('category', 'id');

        return $query;

    }

    /**
     * Get all items
     *
     * @return \Illuminate\Support\Collection
     */
    public static function get() {

        $query = DB::table(self::$table)
            ->select('*')
            ->orderBy('created_at', 'desc')
            ->get();

        return $query;

    }

    /**
     * Get items details
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByID($id) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('id','=', $id)
            ->first();

        return $query;

    }

    /**
     * Check if item exist
     *
     * @return \Illuminate\Support\Collection
     */
    public static function checkIfExist($param) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('category','=', $param->category)
            ->get();

        return (count($query)) ? true : false ;

    }

    /**
     * Check category does not exist
     *
     * @return \Illuminate\Support\Collection
     */
    public static function checkIfValidCategory($param) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('category','=', $param->category)
            ->where('id','<>', $param->itemid)
            ->get();

        return (!count($query)) ? true : false ;

    }
}