<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-22
 * Time: 12:21
 */
namespace App\Models;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Query;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class TodoListModel extends Controller
{
    protected static $responseUser;
    protected static $responseTodos;


    public function __construct(){

        $responseUser = Http::get('https://jsonplaceholder.typicode.com/users');
        $responseTodo = Http::get('https://jsonplaceholder.typicode.com/todos');

        if (!$responseUser->failed()) {
            self::$responseUser = $responseUser->json();
        }

        if (!$responseTodo->failed()) {
            self::$responseTodos = $responseTodo->json();
        }
    }

    /**
     * Get items by sort - MABU
     *
     * @param $descending
     * @return \Illuminate\Support\Collection
     */
    public static function getBySort ($descending) {

        $result = array();

        foreach (self::$responseUser as $row) {

            $temp['data'] = $row;
            $temp['todosCount'] = collect(self::$responseTodos)->where('userId', $row['id'])->where('completed', 1)->count();
            array_push($result, $temp);
        }

        return collect($result)->sortBy('todosCount', SORT_REGULAR,$descending );
    }

    /**
     * Get item by keywords - MABU
     * 
     * @param $keyword
     * @return Collection
     */
    public static function getByKeyword ($keyword) {

        $collection = collect(self::$responseTodos);
        $keyword = $keyword['keyword'];

        $colTitle = $collection->filter(function ($q) use ($keyword) {
            return Str::startsWith($q['title'],$keyword);
        });

        $colUserId = $collection->filter(function ($q) use ($keyword) {
            return Str::startsWith($q['userId'],$keyword);
        });

        return $colTitle->merge($colUserId);
    }
}