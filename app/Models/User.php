<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class User extends Controller
{
    protected static $table = 'user';

    /**
     * Insert new item
     *
     * @param $param
     * @return bool
     */
    public static function create($param) {

        $result = DB::table(self::$table)
            ->insert(array('first_name' => $param->firstname, 'last_name' => $param->lastname, 'email' => $param->email, 'created_at' => date('Y-m-d H:i:s')));

        return $result;
    }

    /**
     * update item
     *
     * @param $param
     * @return int
     */
    public static function update($param) {

        $result = DB::table(self::$table)
            ->where('id', '=',$param->itemid)
            ->update(array('first_name' => $param->firstname, 'last_name' => $param->lastname, 'email' => $param->email, 'updated_at' => date('Y-m-d H:i:s')));

        return $result;

    }

    /**
     * delete item
     *
     * @param $param
     * @return int
     */
    public static function delete($id) {

        $result = DB::table(self::$table)
            ->where('id', '=',$id)
            ->delete();

        return $result;

    }

    /**
     * Check if item exist
     *
     * @return \Illuminate\Support\Collection
     */
    public static function checkIfExist($param) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('email','=', $param->email)
            ->get();

        return (count($query)) ? true : false ;

    }

    /**
     * Get all items
     *
     * @return \Illuminate\Support\Collection
     */
    public static function get() {

        $query = DB::table(self::$table)
            ->select('*')
            ->orderBy('created_at', 'desc')
            ->get();

        return $query;

    }

    /**
     * Get items details
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getByID($id) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('id','=', $id)
            ->first();

        return $query;

    }

    /**
     * Check email does not exist
     *
     * @return \Illuminate\Support\Collection
     */
    public static function checkIfValidEmail($param) {

        $query = DB::table(self::$table)
            ->select('*')
            ->where('email','=', $param->email)
            ->where('id','<>', $param->itemid)
            ->get();

        return (!count($query)) ? true : false ;

    }
}
