<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-23
 * Time: 17:58
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.  - MABU
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:100',
            'lastname'  => 'required|max:100',
            'email'     => 'required|email'
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
