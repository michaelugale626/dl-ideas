<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-22
 * Time: 12:10
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.  - MABU
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keyword' => 'required|max:100'
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
