<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-26
 * Time: 18:06
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.  - MABU
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|min:2'
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
