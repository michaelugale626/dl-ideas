<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-26
 * Time: 20:42
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.  - MABU
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task'          => 'required|min:2',
            'description'   => 'required|max:200',
            'category'      => 'required|not_in:0'
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
