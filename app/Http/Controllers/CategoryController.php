<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\CategoryModel;
use App\Models\TaskModel;

class CategoryController extends Controller
{
    /**
     * Show all list items
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showListing() {

        return view('admin/category/list')->with('list',CategoryModel::get());
    }

    /**
     * Show create form - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showCreate() {

        return view('admin/category/add');
    }

    /**
     * Show item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showEdit($id) {


        if (!is_numeric($id)) {
            redirect('admin/category/listing')->withErrors('Invalid user id, please try again');
        }

        return view('admin/category/edit')->with('item', CategoryModel::getByID($id));
    }

    /**
     * Update an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processUpdate(CategoryRequest $request) {

        if (CategoryModel::checkIfValidCategory($request)) {
            CategoryModel::update($request);
            return redirect()->back()->withSuccess('Category successfully updated');
        } else {
            return redirect()->back()->withErrors('Invalid category details, please try again');
        }
    }

    /**
     * Delete an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processDelete($id) {

        if (is_numeric($id)) {
            if (TaskModel::checkIfCategoryExist($id)) {
                return redirect()->back()->withErrors('Can not delete, category is currently in used');
            } else {
                CategoryModel::delete($id);
                return redirect()->back()->withSuccess('Category successfully deleted');
            }
        } else {
            return redirect()->back()->withErrors('Invalid Category id, please try again');
        }
    }

    /**
     * Process creating new category
     *
     * @param CategoryRequest $request
     * @return mixed
     */
    public function processCreate(CategoryRequest $request) {

        if (CategoryModel::checkIfExist($request)) {
            return redirect()->back()->withErrors('Category already exist');
        } else {
            CategoryModel::create($request);
            return redirect()->back()->withSuccess('Category successfully created');
        }
    }
}
