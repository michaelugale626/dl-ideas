<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Http\Requests\AddRequest;
use App\Models\User;


class UserManager extends Controller
{

    /**
     * show default/ Dashboard page - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showPage() {

        return view('admin/users/list')->with('list',User::get());
    }

    /**
     * Show all list items
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showListing() {

        return view('admin/users/list')->with('list',User::get());
    }

    /**
     * Show create form - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showCreate() {

        return view('admin/users/add');
    }

    /**
     * Show item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showEdit($id) {


        if (!is_numeric($id)) {
            redirect('admin/listing')->withErrors('Invalid user id, please try again');
        }

        return view('admin/users/edit')->with('item', User::getByID($id));
    }

    /**
     * Update an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processUpdate(AddRequest $request) {

        if (User::checkIfValidEmail($request)) {
            User::update($request);
            return redirect()->back()->withSuccess('User successfully updated');
        } else {
            return redirect()->back()->withErrors('Invalid user details, please try again');
        }
    }

    /**
     * Delete an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processDelete($id) {

        if (is_numeric($id)) {
            User::delete($id);
            return redirect()->back()->withSuccess('User successfully deleted');
        } else {
            return redirect()->back()->withErrors('Invalid user id, please try again');
        }
    }

    /**
     * Process creating new user - MABU
     *
     * @param AddRequest $request
     * @return mixed
     */
    public function processCreate(AddRequest $request) {

        if (User::checkIfExist($request)) {
            return redirect()->back()->withErrors('User already exist');
        } else {
            User::create($request);
            return redirect()->back()->withSuccess('User successfully created');
        }
    }

}
