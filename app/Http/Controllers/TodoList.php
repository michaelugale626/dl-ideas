<?php
/**
 * Created by PhpStorm.
 * User: Michael Ugale
 * Date: 2021-04-21
 * Time: 17:16
 */
namespace App\Http\Controllers;
use App\Http\Requests\SearchRequest;

use App\Models\TodoListModel;
use App\Models\TaskModel;

use Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Query;


class TodoList extends Controller
{
    public function __construct(){

    }

    public function index() {

        $model  = new TodoListModel();

        return View::make('listTodo')->with('list', $model::getBySort(true));
    }

    /**
     * Show page with list to do - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showPage() {

        $model  = new TodoListModel();

        return view('admin/assignment/list')->with('list', $model::getBySort(true));
    }

    /**
     * Search data by keyword - MABU
     *
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed|object
     */
    public function search(SearchRequest $request) {


        $model  = new TodoListModel();

        return $model::getByKeyword($request);
    }

    /**
     * Print values
     *
     * @param $name
     * @return string
     */
    public function showHello($name) {


        return 'Hello ' . $name . '. Nice to meet you';
    }
}