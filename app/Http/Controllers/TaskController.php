<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use App\Models\CategoryModel;
use App\Models\TaskModel;

class TaskController extends Controller
{

    /**
     * Show all list items
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showListing() {

        return view('admin/task/list')->with('list',TaskModel::get());
    }

    /**
     * Show create form - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showCreate() {

        $categories = CategoryModel::getList();
        $categories->prepend('Select', '0');

        return view('admin/task/add')->with('list', $categories);
    }

    /**
     * Show item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showEdit($id) {

        if (!is_numeric($id)) {
            redirect('admin/task/listing')->withErrors('Invalid task id, please try again');
        }

        $categories = CategoryModel::getList();
        $categories->prepend('Select', '0');

        $data = array('list' => $categories, 'item' => TaskModel::getByID($id));

        return view('admin/task/edit')->with('data', $data);
    }

    /**
     * Update an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processUpdate(TaskRequest $request) {

        TaskModel::update($request);
        return redirect()->back()->withSuccess('Task successfully updated');
    }

    /**
     * Delete an item by ID - MABU
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processDelete($id) {

        if (is_numeric($id)) {
            TaskModel::delete($id);
            return redirect()->back()->withSuccess('Task successfully deleted');
        } else {
            return redirect()->back()->withErrors('Invalid task id, please try again');
        }
    }

    /**
     * Process creating new category
     *
     * @param CategoryRequest $request
     * @return mixed
     */
    public function processCreate(TaskRequest $request) {

        TaskModel::create($request);
        return redirect()->back()->withSuccess('Task successfully created');
    }
}
