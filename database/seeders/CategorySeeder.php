<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'category' => 'Low priority',
            'created_at' => now()
        ]);

        DB::table('category')->insert([
            'category' => 'Medium priority',
            'created_at' => now()
        ]);

        DB::table('category')->insert([
            'category' => 'High priority',
            'created_at' => now()
        ]);
    }
}
