<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'first_name' => 'michael',
            'last_name' => 'ugale',
            'created_at' => now(),
            'email' => 'michaelugale626@gmail.com'
        ]);

        DB::table('user')->insert([
            'first_name' => 'mark',
            'last_name' => 'ugale',
            'created_at' => now(),
            'email' => 'markugale@gmail.com'
        ]);
    }
}
